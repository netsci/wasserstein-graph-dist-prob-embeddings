Project: A Wasserstein graph distance based on distributions of probabilistic node embeddings
====================
[![arXiv:2401.03913](https://img.shields.io/badge/arXiv-2401.03913-red?logo=arxiv&color=b31b1b)](https://arxiv.org/abs/2401.03913)
[![Latex Pipeline](https://git.rwth-aachen.de/netsci/wasserstein-graph-dist-prob-embeddings/badges/main/pipeline.svg)](https://git.rwth-aachen.de/netsci/internal-publication-backup/cnp-ot/-/jobs/artifacts/main/file/paper/main.pdf?job=compile-latex)
[![pyPi](https://img.shields.io/badge/pip%20install-"Pending"-blue?logo=pyPi)](https://pypi.org/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://git.rwth-aachen.de/netsci/wasserstein-graph-dist-prob-embeddings/blob/main/LICENSE)


Distance measures between graphs are important primitives for a variety of learning tasks.
In this work, we describe an unsupervised, optimal transport based approach to define a distance between graphs.
Our idea is to derive representations of graphs as Gaussian mixture models, fitted to distributions of sampled node embeddings over the same space.
The Wasserstein distance between these Gaussian mixture distributions then yields an interpretable and easily computable distance measure, which can further be tailored for the comparison at hand by choosing appropriate embeddings.
We propose two embeddings for this framework and show that under certain assumptions about the shape of the resulting Gaussian mixture components, further computational improvements of this Wasserstein distance can be achieved. 
An empirical validation of our findings on synthetic data and real-world Functional Brain Connectivity networks shows promising performance compared to existing embedding methods.

##### Usage
This repository contains all code and data to replicate the experiments from the accompanying paper. You are also invited to use the provided code and packages on your own data under the MIT license. If you publish work that partially uses our code, method or ideas, please cite the above paper.

You can find the Jupyter Notebooks in the `/code` folder for the two experiments, one on synthetic networks and one on the functional connectivity networks (also provided in this repository). The notebooks are self-explanatory and contain comments for available options and parameters.

##### Funding acknowledgments 
This work was funded as part of the Graphs4Patients Consortia by the BMBF
(Bundesministerium fur Bildung und Forschung). We acknowledge partial ¨
funding from the DFG RTG 2236 “UnRAVeL” – Uncertainty and Randomness in Algorithms, Verification and Logic.” and the Ministry of Culture and
Science of North Rhine-Westphalia (NRW Ruckkehrprogramm).
